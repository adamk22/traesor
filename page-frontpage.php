<?php
/**
 * Template Name: Frontpage Page
 * Description: Traesor Frontpage
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render("page-frontpage.twig", $context );